'use strict';

angular.module('myApp.user.service', [])
.service('UserService', function ($http, API_URL) {
  var postData = function (data) {
    var req = {
      method: 'POST',
      url: API_URL,
      data: data
    };
    return $http(req);
  };

  var getData = function (data) {
    var req = {
      method: 'GET',
      url: API_URL
    };
    return $http(req, data);
  };

  var delData = function (id) {
    var req = {
      method: 'DELETE',
      url: API_URL + '/' + id,
    };
    return $http(req);
  };

  var putData = function (data, id) {
    var req = {
      method: 'PUT',
      url: API_URL + '/' + id,
      data: data
    };
    return $http(req);
  };

  return {
    post: function (params) {
      return postData(params);
    },
    get: function () {
      return getData();
    },
    del: function (id) {
      return delData(id);
    },
    put: function (data, id) {
      return putData(data, id);
    }
  };

});
