'use strict';

angular.module('myApp', [
  'ngRoute',
  'myApp.user'
])

.constant('API_URL', 'http://tidy-api-test.herokuapp.com/api/v1/customer_data')

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.otherwise({redirectTo: '/view1'})
}]);
