'use strict';

describe('myApp.user module', function() {


  beforeEach(module('myApp'));
  beforeEach(module('myApp.user'));
  beforeEach(module('myApp.user.service'));

  var UserCtrlList,
    $UserService,
    scope,
    call,
    rootScope,
    http;

  beforeEach(inject(function ($controller, $rootScope, UserService, $httpBackend, _$window_, $q ) {
    scope = $rootScope.$new();
    $UserService = UserService;
    rootScope = $rootScope;
    UserCtrlList = $controller('UserCtrlList', {
      $scope: scope
    });
    http = $httpBackend;
    spyOn(UserService, 'get').and.callThrough();

  }));

  describe('list', function(){

    it('should be defined', inject(function($controller) {
      var userCtrlList = $controller('UserCtrlList');
      expect(userCtrlList).toBeDefined();
    }));

    it('should be called', function(done) {
      http.expectGET('http://tidy-api-test.herokuapp.com/api/v1/customer_data').respond({}, '');
      http.flush();
      expect(UserCtrlList.users).not.toBe(null);
      rootScope.$digest();
      done();
    });

  });

});
