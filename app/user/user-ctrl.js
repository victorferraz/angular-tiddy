'use strict';

(function() {
  angular.module('myApp.user', ['ngRoute', 'myApp.user.service'])
  .config(['$routeProvider', function($routeProvider) {
    $routeProvider
      .when('/list', {
        templateUrl: 'user/user-list.html'
      })
      .when('/delete/:id', {
        templateUrl: 'user/user-delete.html'
      })
      .when('/add', {
        templateUrl: 'user/user-add.html'
      })
      .when('/edit/:id', {
        templateUrl: 'user/user-edit.html'
      })
  }])
  .controller('UserCtrlList', UserCtrlList)
  .controller('UserCtrlEdit', UserCtrlEdit)
  .controller('UserCtrlAdd', UserCtrlAdd)
  .controller('UserCtrlDelete', UserCtrlDelete)

  function UserCtrlList (UserService, $routeParams) {
    var vm = this;
    vm.users = {};
    UserService.get()
    .then( function (response) {
        vm.users = response.data;
      }, function (response) {
    });
  }

  function UserCtrlEdit (UserService, $routeParams) {
    var vm = this;
    vm.users = {};
    vm.id = $routeParams.id;
    UserService.get()
    .then( function (response) {
        vm.user = findUser(response.data, vm.id)[0];
      }, function (response) {
    });

    vm.submit = function () {
      UserService.put(vm.user, vm.id)
      .then(function (response) {
          vm.msg = 'Data was saveld';
        }, function (response) {
          vm.msg = 'Problem on save, try again';
      });
    };

  }

  function UserCtrlAdd (UserService, $routeParams) {
    var vm = this;
    vm.user = {};

    vm.submit = function () {
      UserService.post(vm.user)
      .then(function (response) {
          vm.msg = 'Data was saveld';
        }, function (response) {
          vm.msg = 'Problem on save, try again';
      });
    };

  }



  function UserCtrlDelete (UserService, $routeParams) {
    var vm = this;
    vm.user = {};
    vm.id = $routeParams.id;

    vm.delete = function () {
      UserService.del(vm.id)
      .then(function (response) {
          vm.msg = 'Data was deleted';
        }, function (response) {
          vm.msg = 'Problem on exclude, try again';
      });
    };

  }

  function findUser(data, id) {
    return data.filter(function (index) {
        return index.id == id;
    });

  }

})();
